IST - Eden Box - LEIC

Autoria e responsabilidade:
	Tiago Gonçalves 
	Carolina Vitorino
	Artur Fortunato
	André Pereira
	Nuno Amaro
	Pedro Maximiliano
	João Antunes
	
Agradecimento por conteúdo cedido:
	Gabriel Maia
	Joel Almeida
	Daniel Correia
	Filipa Pedrosa
	Pedro Bucho
	Nuno Amaro
	Pedro Cunha
	Tomás Duarte
	Pedro Orvalho
	João Silveira
	Rodrigo Mira
	João Henriques
	Vasco Jorge
	Nádia Mourão
	Afonso Gonçalves
	Baltasar Dinis
	Patrícia Silva
	Jorge Godinho
	Ana Evans
	Tiago Almeida
	Duarte Santos

Agradeço ainda aos amigos que ajudaram a criar o logótipo deste projeto:
    Miguel Francisco
    Davide Costa
    Tiago Costa
    André Gonçalves


Contribuição de conteúdos para:
contribution@edenbox.cf

Para colaborar, esclarecimento de dúvidas ou outras informações contactar:
	Tiago Gonçalves: https://www.facebook.com/tiago.gon0xE7alves
	general@edenbox.cf
